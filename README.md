# DaliaTestAutomation


**How to run the script** 

* Test file is mentioned in package.json file and you can run from this below line
*  "scripts": {
    "test": "node_modules/.bin/mocha --timeout 15000 ./src/test/**answerTheSurvey.js** --ignore-certificate-errors-spki-list --ignore-ssl-errors --reporter mochawesome"
  },
  
**About the Framework :**
* I have created the automation script with Mocha framework along with Selenium and Javascript
* Created Page object module to reuse the functionalities
* Test reported is generated in mochaAwesome liabray
* Data is retrived from retrieveData folder

