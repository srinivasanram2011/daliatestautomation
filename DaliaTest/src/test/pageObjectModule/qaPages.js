const {By,until } = require('selenium-webdriver');
const{expect,assert}=require('chai');

module.exports= function PageObj() {
    const pageObjects = {
        getNotification:By.xpath('//*[@ng-if="notifyMessage"]'),
        selectAnswer:By.xpath('//*[@class="answer_section"]//following::div[@class="question_option question_option_background_hover ng-scope"]'),
        getQuestion: By.xpath('//*[@title="question.title"]'),
        clickNextBtn: By.xpath('//*[@class="next_button_a ng-scope"]//button'),
        blogId:By.xpath('//*[@for="answer_3_O0050"]'),
        questionId:By.xpath('//*[@question-id="answer_3"]'),
        fourthQn:By.xpath('//*[@question-id="answer_4"]'),
        fifthQn:By.xpath('//*[@question-id="answer_5"]'),
        sixthQn:By.xpath('//*[@question-id="answer_6"]'),
        seventhQn:By.xpath('//*[@question-id="answer_7"]'),
        eighthQn:By.xpath('//*[@question-id="answer_8"]'),
        ninthQn:By.xpath('//*[@question-id="answer_9"]'),
        tenthQn:By.xpath('//*[@question-id="answer_10a"]'),
        secretsToLearn:By.xpath('//input[@ng-blur="lazyLoad=true"]'),
        EndMessage:By.xpath('//*[@class="col-md-12"]'),
        successMsg:By.xpath('//*[@class="support-link"]')

    };

    return {
        checkQn2:pageObjects.getNotification,
        checkQn3:pageObjects.blogId,
        checkQn4:pageObjects.fourthQn,
        checkQn5:pageObjects.fifthQn,
        checkQn6:pageObjects.sixthQn,
        checkQn7:pageObjects.seventhQn,
        checkQn8:pageObjects.eighthQn,
        checkQn9:pageObjects.ninthQn,
        checkQn10:pageObjects.tenthQn,
        checkDoneMessage:pageObjects.successMsg,


        radioButtonSelection:async function(driver,clickRadioToAnswer,answer){
            await driver.wait(until.elementLocated(clickRadioToAnswer),50000).getText().then(async function (notification) {
                await driver.findElement(pageObjects.getQuestion).getText().then(function (getQuestion2) {
                    console.log(getQuestion2);
                });

            }).then(async function () {
               // await driver.wait(until.elementLocated(pageObjects.selectAnswer),3000);
                await driver.findElements(pageObjects.selectAnswer).then(async function (count) {
                    await driver.executeScript("window.scrollBy(0,750)");
                   console.log('radiosize', count.length);
                        for(let i=1;i<=count.length;i++){
                        await driver.findElement(By.xpath('(//div[@class="question_option question_option_background_hover ng-scope"])['+i+']')).getText().then(async function (ratioOptions) {
                            if(ratioOptions===answer){
                                await driver.findElement(By.xpath('(//div[@class="question_option question_option_background_hover ng-scope"])['+i+']')).click().then(async function () {
                                    await driver.findElement(pageObjects.clickNextBtn).click();

                                });

                            }

                        });
                    }
                });

            });

        },
        checkBoxes:async function(driver,blogCheck){
            this.resolveAfter2Seconds();
            await driver.wait(until.elementLocated(blogCheck),50000).then(async function (blog) {
            await driver.findElement(pageObjects.getQuestion).getText().then(async function (getQuestion2) {
            console.log(getQuestion2);
                await driver.executeScript("window.scrollBy(0,1000)");
                });
             await driver.findElements(pageObjects.selectAnswer).then(async function (checkBoxOption) {
                 for (let i = 1; i <= checkBoxOption.length; i++) {
                     await driver.findElement(By.xpath('(//div[@class="question_option question_option_background_hover ng-scope"])[' + i + ']')).getText().then(async function (checkBoxText) {
                         if (checkBoxText === 'Facebook' || checkBoxText === 'YouTube' || checkBoxText === 'LinkedIn') {
                             console.log('counting',i);

                             await driver.findElement(By.xpath('(//div[@class="question_option question_option_background_hover ng-scope"])[' + i + ']')).click().then(async function () {
                                if(i===2){
                                    await driver.findElement(pageObjects.clickNextBtn).getCssValue('background-color').then(async function (EditableColor) {
                                        expect('rgba(65, 175, 165, 1)').to.equal(EditableColor);
                                        await driver.findElement(pageObjects.clickNextBtn).click();
                                    }).then(() => {
                                        console.log('PASSED')
                                    })
                                        .catch((err) => {
                                            console.log('Next button is not enabled when expected 2 answer is been checked', err)
                                        });
                                }
                                 await driver.findElement(pageObjects.clickNextBtn).click();


                             });

                         } else if (checkBoxText === 'Newspaper') {
                             await driver.findElement(By.xpath('(//div[@class="question_option question_option_background_hover ng-scope"])[' + i + ']')).click().then(async function () {
                                 await driver.findElement(pageObjects.clickNextBtn).click();
                                 await driver.executeScript("window.scrollBy(0,1000)");


                             });
                         }


                     });


                 }
             });



        });
            },
        enterTextToAnswer:async function(driver,questionFlag,value){
            this.resolveAfter2Seconds();
            await driver.wait(until.elementLocated(questionFlag),10000).then(async function () {

                    await driver.findElement(pageObjects.getQuestion).getText().then(function (getQuestion2) {
                        console.log(getQuestion2);
                    });
                    });
                    await driver.wait(until.elementLocated(pageObjects.secretsToLearn),3000).sendKeys(value);
                    await driver.findElement(pageObjects.clickNextBtn).click();

                    },

        verifyEndMessage:async function(driver,AllDone){
            this.resolveAfter2Seconds();
            console.log('wellDone',AllDone);
            await driver.wait(until.elementIsVisible(AllDone),10000).then(function (AllDoneText) {
                console.log('Done',AllDoneText);
            });

        },

        resolveAfter2Seconds:function(){
            return new Promise(resolve => {
                setTimeout(() => {
                    resolve('resolved');
                }, 5000);
                console.log('waited for 2000ms');
            });
        },
    }


    };

