const {By,until } = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');

module.exports= function PageObj() {
    const pageObjects = {
        clickCheckBox:By.css('.answer_section'),
        clickNextBtn: By.xpath('//*[@class="next_button_a ng-scope"]//button'),

    };

    return {

        chromeCap: async function () {
            const chromeOptions = new chrome.Options();
            chromeOptions.addArguments('--ignore-certificate-errors-spki-list');
            chromeOptions.addArguments('--ignore-ssl-errors');
            chromeOptions.addArguments('--no-sandbox');
            chromeOptions.addArguments('window-size=1200,1000');
            chromeOptions.addArguments('--disable-gpu');

        },

        checkBoxSelection:async function(driver){
            await driver.findElement(pageObjects.clickCheckBox).click();
            this.resolveAfter2Seconds();


        },
        clickNext:async function(driver){
            await driver.wait(until.elementLocated(pageObjects.clickNextBtn),15000).isEnabled().then(async function (state) {
                await driver.findElement(pageObjects.clickNextBtn).click();
                await driver.executeScript("window.scrollBy(0,1000)");

            });


                   },
        resolveAfter2Seconds:function(){
            return new Promise(resolve => {
                setTimeout(() => {
                    resolve('resolved');
                }, 2000);
                console.log('waited for 2000ms');
            });
        },



    };






};