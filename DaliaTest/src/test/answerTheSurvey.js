'use strict';

let webdriver = require('selenium-webdriver');
let chromeCapabilities = webdriver.Capabilities.chrome();
require('chromedriver');
let mocha = require('mocha');
let describe = mocha.describe;
let it = mocha.it;
const fs=require('fs');


const welcomePage=require('./pageObjectModule/welcomePage.js');
const questionPage=require('./pageObjectModule/qaPages.js');
let contents = fs.readFileSync('./src/test/retrieveData/dateFile.json');
let jsonContent = JSON.parse(contents);
let url = jsonContent.url;

chromeCapabilities.set('chromeOptions', {'args': [ '--ignore-certificate-errors-pki-list','--ignore-ssl-errors','--no-sandbox', 'window-size=1200,1000' , '--disable-gpu'] });
const driver=new webdriver.Builder().forBrowser("chrome").withCapabilities(chromeCapabilities).build();

describe('Test_1',function () {
    let homePage=new welcomePage();
    let questionPages=new questionPage();

    it('Verify Yuno welcome page',function (done) {
         driver.get(url).then(async function () {
            await driver.manage().window().maximize(1200, 800);
           await  homePage.checkBoxSelection(driver);
           await homePage.clickNext(driver);
        });
        this.timeout(15000);
        setTimeout(done,300);
    });
    it('Verify Q n A functionality', async function () {

         await questionPages.radioButtonSelection(driver, questionPages.checkQn2, '1-3 hours');
        await questionPages.checkBoxes(driver,questionPages.checkQn3);
        await questionPages.checkBoxes(driver,questionPages.checkQn4);
        await questionPages.enterTextToAnswer(driver,questionPages.checkQn5,"Vinoth");
        await questionPages.radioButtonSelection(driver,questionPages.checkQn6,'Neither agree nor disagree');
        await questionPages.enterTextToAnswer(driver,questionPages.checkQn7,"7");
       await questionPages.radioButtonSelection(driver,questionPages.checkQn8,'City');
       await questionPages.radioButtonSelection(driver,questionPages.checkQn9,'The Godfather');
       await questionPages.radioButtonSelection(driver,questionPages.checkQn10,'Cinematography');
       await questionPages.verifyEndMessage(driver,questionPages.checkDoneMessage);

       // this.timeout(15000);
       // setTimeout(done,300);
    });
});